import { BezierController } from './src/bezier/bezier.controller.js';
import { Casteljau } from './src/controlers/casteljau.controller.js';
import { Logger } from './src/logger.js';
import { App } from './src/controlers/app.controller.js';


const root = document.getElementById('root');

const bezierController = new BezierController();
const casteljauController = new Casteljau();

casteljauController.onCasteljauMove((t) => bezierController.makeCasteljauMovement(t));

const logger = new Logger();
const app = new App();

bezierController.init(root);
logger.init();

app
	.listen('start', () => casteljauController.startCasteljauMovement())
	.listen('stop', () => casteljauController.stopCasteljauMovement())
	.listen('reset', () => casteljauController.resetCasteljauMovement())
	.listen('addPoint', () => bezierController.addPivotPoint());

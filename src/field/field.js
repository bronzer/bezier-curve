import { CONSTANTS } from '../common.js';
import { BezierElement } from '../bezier/bezier-element.js';


export class Field extends BezierElement {

	constructor () {

		super();

		const { field, fieldWrapper } = this._createElement();

		this._elementWrapper = fieldWrapper;
		this._element = field;

	}

	get wrapper () {

		return this._elementWrapper;

	}

	get fieldParams () {

		const {
			width: fieldWidth,
			height: fieldHeight,
			top: fieldTop,
			left: fieldLeft,
		} = this._element.getBoundingClientRect();
		const firstPointCenter = CONSTANTS.SVG_PADDING + CONSTANTS.POINT.radius;
		const fieldBottom = fieldHeight - firstPointCenter;

		return {
			fieldWidth,
			fieldBottom,
			fieldHeight,
			fieldTop,
			fieldLeft,
			firstPointCenter,
		};

	}

	addElement (elem) {

		this._element.appendChild(elem);

	}

	_createElement () {

		const field = document.createElementNS(CONSTANTS.SVG_NS, 'svg');
		const fieldWrapper = document.createElement('div');

		fieldWrapper.classList.add('svgWrapper');
		fieldWrapper.appendChild(field);

		return { field, fieldWrapper };

	}
}

import { Field } from './field.js';


export class FieldController {

	static createField () {

		return new Field();

	}

	static appendToContainer (field, container = null) {

		if (container !== null) {

			container.appendChild(field.wrapper);

		}

	}

	static addElement (field, point = null) {

		field.addElement(point);

	}

}

//TODO change to proxy
export class ArrayWatcher extends Array {

    constructor () {
        super();

        this.watchers = [];
        this.lastUpdate = null;
    }

    addWatcher (watcher) {

        this.watchers.push(watcher);

        return this;

    }

    push (...items) {

        super.push(...items);

        this.lastUpdate = items;

        return this;

    }

    update (...items) {

        this.push(...items);
        this.notify();

        return this;

    }

    notify () {

        this.watchers.forEach((watcher) => {
            if (typeof watcher === 'function') {
                watcher(this.lastUpdate);
            }
        });

        return this;

    }

}

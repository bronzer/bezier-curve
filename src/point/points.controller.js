import { CONSTANTS } from '../common.js';
import { Point } from './point.js';
import { GuidePoint } from './guide-point.js';


export class PointsController {

	static getDefaultPoints (fieldParams) {

		const { fieldWidth, fieldBottom, firstPointCenter } = fieldParams;

		const rawPoints = [
			{
				x: firstPointCenter,
				y: fieldBottom,
			}, {
				x: fieldWidth - firstPointCenter,
				y: firstPointCenter,
			},
		];

		return rawPoints.map((pointRaw) => new Point(pointRaw));

	}

	static removeGuidePoints (guidePoints) {

		guidePoints.forEach(guidePoint => guidePoint.destroy());

	}

	/**
	 * Creating new point and moving all of them to default position
	 * @returns {Point}
	 */

	static createPoint () {

		return new Point({ x: 0, y: 0 });

	}

	static placePoints (points, fieldParams) {

		const isEvenLength = points.length % CONSTANTS.HALF === 0;

		if (isEvenLength) {

			PointsController.placeEvenAmountOfPoints(points, fieldParams);

		} else {

			PointsController.placeOddAmountOfPoints(points, fieldParams);

		}

	}

	/**
	 if amount of point (after adding) is even,
	 put half of them in top and half in bottom
	 */
	static placeEvenAmountOfPoints (points, { fieldWidth, firstPointCenter, fieldBottom }) {

		/** how many pivotPoints will be placed on each side*/
		const halfPointsAmount = points.length / CONSTANTS.HALF;
		const fieldMiddle = fieldWidth / CONSTANTS.HALF;
		/** length of line where point will be put one by one */
		const lineLength = fieldMiddle - firstPointCenter;
		const spacesBetweenPoints = halfPointsAmount > 2 ? halfPointsAmount - 1 : 1;
		/** distance between pivotPoints' centers */
		const distance = lineLength / spacesBetweenPoints;

		// bottom side (begins left field side and stops in the middle)
		// top side (begins from middle of the field)
		points.forEach((point, i) => {

			const isBottomPoint = i < halfPointsAmount;

			// if top point, calculate index as it begins from 0
			const index = isBottomPoint ? i : (halfPointsAmount - i) * -1;
			const beginFrom = isBottomPoint ? firstPointCenter : fieldMiddle;

			point.position = {
				x: index === 0 ? beginFrom : beginFrom + distance * index,
				y: isBottomPoint ? fieldBottom : firstPointCenter,
			};

		});

	}

	static placeOddAmountOfPoints (points, { fieldWidth, firstPointCenter, fieldBottom }) {

		/** length of line where point will be put one by one */
		const lineLength = fieldWidth - firstPointCenter * 2;
		// conditions begins from 3 pivotPoints
		const spacesBetweenPoints = points.length - 1;
		/** distance between pivotPoints' centers */
		const distance = lineLength / spacesBetweenPoints;

		points.forEach((point, i) => {

			const isTopPoint = (i + 1) % 2 === 0;

			point.position = {

				x: firstPointCenter + distance * i,
				y: isTopPoint ? firstPointCenter : fieldBottom,

			};

		});

	}

	static setPointPosition ({ clientX, clientY }, point, fieldParams) {

		const { fieldTop, fieldLeft, fieldWidth, fieldHeight } = fieldParams;
		const { top: topE, left: leftE, width: widthE, height: heightE } = point.element.getBoundingClientRect();

		const boundaries = CONSTANTS.SVG_PADDING + CONSTANTS.POINT.radius;

		const isClientXInBoundaries = clientX >= fieldLeft + boundaries &&
			clientX <= fieldLeft + fieldWidth - boundaries;
		const isClientYInBoundaries = clientY >= fieldTop + boundaries &&
			clientY <= fieldTop + fieldHeight - boundaries;

		// Move by X axis
		if (isClientXInBoundaries && point !== null) {

			const centreByPage = leftE + widthE / CONSTANTS.HALF;

			point.position = { x: clientX - (centreByPage - point.position.x) };

		}

		// Move by Y axis
		if (isClientYInBoundaries && point !== null) {

			const centreByPage = topE + heightE / CONSTANTS.HALF;

			point.position = { y: clientY - (centreByPage - point.position.y) };

		}

	}

	static createGuidePoint (coordinates = { x: 0, y: 0 }) {

		return new GuidePoint(coordinates);

	}

	static getPointInstance (points, pointElement) {

		const pointsFiltered = points.filter((point) => point.element === pointElement);

		if (pointsFiltered.length > 0) {

			return pointsFiltered[ 0 ];

		}

		return null;

	}

}

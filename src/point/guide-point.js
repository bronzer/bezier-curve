import { Point } from './point.js';
import { CONSTANTS } from '../common.js';


export class GuidePoint extends Point {

	constructor (coordinates, props = {}) {

		super(coordinates, {
			...CONSTANTS.GUIDE_POINT,
			...props,
		});

	}

}

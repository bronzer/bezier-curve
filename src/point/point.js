import { CONSTANTS } from '../common.js';
import { BezierElement } from '../bezier/bezier-element.js';


export class Point extends BezierElement {

	get position () {

		return {
			x: parseInt(this._element.getAttribute(`cx`), CONSTANTS.RADIX),
			y: parseInt(this._element.getAttribute(`cy`), CONSTANTS.RADIX),
		};

	}

	set position (coordinates) {

		const { x = this.position.x, y = this.position.y } = coordinates;

		this._element.setAttributeNS(null, 'cx', `${x}`);
		this._element.setAttributeNS(null, 'cy', `${y}`);

		this.handlers.forEach(handler => handler(x, y));

	}

	constructor (coordinates, props = {}) {

		super({ ...CONSTANTS.POINT, ...props });

		this.handlers = [];
		this._element = this._createElement();
		this.position = coordinates;

	}

	onPositionChange (handler) {

		if (typeof handler === 'function') {

			this.handlers.push(handler);

		}

	}

	_createElement () {

		const point = document.createElementNS(CONSTANTS.SVG_NS, 'circle');

		point.setAttributeNS(null, 'r', `${this._renderProps.radius}`);
		point.setAttributeNS(null, 'stroke', this._renderProps.strokeColor);
		point.setAttributeNS(null, 'stroke-width', `${this._renderProps.strokeWidth}`);
		point.setAttributeNS(null, 'fill', this._renderProps.fill);
		point.style.cursor = 'pointer';

		return point;

	}

}

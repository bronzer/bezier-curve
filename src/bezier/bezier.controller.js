import { FieldController } from '../field/field.controller.js';
import { PointsController } from '../point/points.controller.js';
import { Dropper } from '../controlers/drag-n-drop.controller.js';
import { LineController } from '../line/line.controller.js';
import { ArrayWatcher } from '../arrayWatcher.js';


/**
 * Controls flow of points and lines related to field
 */

export class BezierController {

	constructor () {

		/** amount of orders, each two pivotPoints has line, each line has guide point*/
		this.orders = 0;
		/** base instance with Field element */
		this.field = FieldController.createField();
		/** starter points, can be moved by mouse */
        this.pivotPoints = new ArrayWatcher();
		// this.pivotPoints = new Proxy([], {
		// 	set (target, prop, value) {
        //
		// 	}
		// });
		/** order of points, every point position depends on 't' coefficient */
		this.pointsOrders = [];
		/** orders of lines, every order based on two points from pointsOrders */
		this.lineOrders = [];

	}

	/**
	 * Initialize the flow, registers Field element in DOM,
	 * creates pivot points and sets watcher for them.
	 *
	 * @param container {Element} - field wrapper
	 */

	init (container = document.body) {

		FieldController.appendToContainer(this.field, container);

		const points = PointsController.getDefaultPoints(this.field.fieldParams);

		/** subscribe to pivotPoints push updates */
		this.pivotPoints
			.addWatcher((newPoints) => this._onPointsAdding(newPoints))
			.update(...points);

		// this.curve = new BezierCurve(
		//     this.pivotPoints[0],
		//     this.pivotPoints[this.pivotPoints.length - 1],
		//     this.pointsOrders[this.pointsOrders.length - 1],
		// );
		//
		// this.fieldController.addLine(this.curve.element);

	}

	/**
	 * Make one movement for all points, depending on 't' coefficient
	 *
	 * @param t {number} - coefficient (percent of line length)
	 */

	makeCasteljauMovement (t = 0) {

		this.lineOrders.forEach((lines, order) => {
			lines.forEach((line, i) => {

				const length = line.length * t;

				this.pointsOrders[ order ][ i ].position = line.getPointAtLength(length);

			});
		});

		// this.curve.setPosition();

	}

	/**
	 * Add new pivot point to flow
	 */

	addPivotPoint () {

		const newPoint = PointsController.createPoint();

		this.pivotPoints.push(newPoint);
		PointsController.placePoints(this.pivotPoints, this.field.fieldParams);
		/** notify watchers that array was changed */
		this.pivotPoints.notify();

	}

	/**
	 * Executes on clicking "add point", calls _onPointCreation fn,
	 * calls recalculation of all lineOrders, pivotPoints etc.
	 *
	 * @param newPoints {Point[]} - array of pivotPoints
	 * @private
	 */

	_onPointsAdding (newPoints) {

		newPoints.forEach((newPoint) => this._onPointCreation(newPoint));

		if (this.pivotPoints.length > 1) {

			this._resetField();
			this._calculateElements();

		}

	}

	/**
	 * Executes on adding new point, adds it to the field, listens for drag'n'drop
	 *
	 * @param newPoint {Point} - new point that was added
	 * @private
	 */

	_onPointCreation (newPoint) {

		//optimize getter
		const elem = newPoint.element;

		FieldController.addElement(this.field, elem);
		Dropper.setListener(elem, (e, pointElement) => this._handlePivotPointMove(e, pointElement));

	}

	/**
	 * Creates new lines and points. Calculates their positions.
	 *
	 * @param points {Point[]} - default pivot points. After first loop points order in recursion.
	 * @private
	 */
	// TODO REFACTOR ME
	_calculateElements (points = this.pivotPoints) {

		// Lines creation flow

		const currentLines = [];

		points.forEach((point, i, arr) => {

			const nextPoint = arr[ i + 1 ];

			if (typeof nextPoint !== 'undefined') {

				// TODO set one creation method with different colors
				if (this.orders === 0) {

					currentLines.push(LineController.createLine(point, nextPoint));

				} else {

					currentLines.push(LineController.createGuideLine(point, nextPoint));

				}

			}

		});

		currentLines.forEach((line) => FieldController.addElement(this.field, line.element));

		this.lineOrders[ this.orders ] = currentLines;

		// Guide pivotPoints creation flow

		this.pointsOrders[ this.orders ] = currentLines.map((line) => {

			const coordinates = line.getPointAtLength(0);

			return PointsController.createGuidePoint(coordinates);

		});

		const currentGuidePoints = this.pointsOrders[ this.orders ];

		currentGuidePoints.forEach((guidePoint) => FieldController.addElement(this.field, guidePoint.element));

		if (currentGuidePoints.length > 1) {

			this.orders++;

			this._calculateElements(currentGuidePoints);

		}

	}

	/**
	 * Set field and it's elements to default state,
	 * removes all lines and points (except pivot points)
	 *
	 * @private
	 */

	_resetField () {

		this.orders = 0;

		this.lineOrders.forEach((lines) => LineController.removeLines(lines));
		this.pointsOrders.forEach((guidePoints) => PointsController.removeGuidePoints(guidePoints));

		this.lineOrders = [];
		this.pointsOrders = [];

	}

	/**
	 * On pivot point drag'n'drop, set new position depending on cursor
	 *
	 * @param e {Event} - mouse event
	 * @param pointElement {Element} - element of Point instance
	 * @private
	 */

	_handlePivotPointMove (e, pointElement) {

		const point = PointsController.getPointInstance(this.pivotPoints, pointElement);

		if (point !== null) {

			PointsController.setPointPosition(e, point, this.field.fieldParams);

		}

	}

}

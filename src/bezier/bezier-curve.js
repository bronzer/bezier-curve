import { BezierElement } from './bezier-element.js';
import { CONSTANTS } from '../common.js';


export class BezierCurve extends BezierElement {

	set position (coordinates) {

		const {
			x,
			y,
		} = coordinates;

		let path = this._element.getAttribute('d') || '';

		if (path.indexOf('M') > -1) {

			path += `L ${x} ${y},`;

		} else {

			path = `M${x} ${y}`;

		}

		this._element.setAttributeNS(null, 'd', path);

		if (coordinates.x === this.lastPoint.position.x && coordinates.y === this.lastPoint.position.y) {
			this._element.setAttributeNS(null, 'd', '');
		}

	}

	constructor (firstPoint, lastPoint, guidePoint) {

		super(CONSTANTS.BEZIER_CURVE);

		this.firstPoint = firstPoint;
		this.lastPoint = lastPoint;
		this.guidePoint = guidePoint;

		this._element = this._createElement();

		this.position = this.firstPoint.position;

	}

	setPosition () {

		const { x, y } = this.guidePoint.position;

		this.position = {
			x, y,
		};

	}


	_createElement () {

		const line = document.createElementNS(CONSTANTS.SVG_NS, 'path');

		line.setAttribute('stroke', this._renderProps.strokeColor);
		line.setAttribute('stroke-width', `${this._renderProps.strokeWidth}`);
		line.setAttribute('fill', `${this._renderProps.fill}`);
		line.setAttribute('stroke-linejoin', `round`);
		line.setAttribute('stroke-linecap', `round`);

		return line;

	}


}

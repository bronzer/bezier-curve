import { CONSTANTS } from '../common.js';


/**
 * Abstract class for all elements of bezier controller
 */
export class BezierElement {

    /**
     * getter for an DOM element
     * @return {null | Object} - DOM element
     */
    get element () {

        return this._element;

    }

    constructor (props = {}) {

        this._element = null;
        this._renderProps = {
            ...CONSTANTS.DEFAULT_ELEMENT,
            ...props,
        };

    }

    /**
     * Remove element from DOM
     */
    destroy () {

        this._element.parentNode.removeChild(this._element);

    }

}

export const CONSTANTS = {
	HALF: 2,
	RADIX: 10,
	SVG_NS: 'http://www.w3.org/2000/svg',
	SVG_PADDING: 10,
	DEFAULT_ELEMENT: {
		strokeColor: '#333333',
		strokeWidth: 1,
	},
	POINT: {
		radius: 5,
		fill: '#000000',
	},
	GUIDE_POINT: {
		radius: 5,
		strokeColor: '#ff3333',
		strokeWidth: 1,
		fill: '#ff3333',
	},
	LINE: {},
	GUIDE_LINE: {
		strokeColor: '#ff3333',
		strokeWidth: 1,
	},
	BEZIER_CURVE: {
		strokeColor: '#ff3333',
		strokeWidth: 4,
		fill: 'transparent',
	},
	LOGGER: {
        position: 'fixed',
        zIndex: '999',
        top: '15px',
		right: '15px',
		height: '60px',
		width: '60px',
	},
};

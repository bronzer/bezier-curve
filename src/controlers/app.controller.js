/**
 * Controller that manages all buttons and their handlers
 */
export class App {

    /**
     * Shows "Stop" button and runs handler
     * @param {Event} e - "click" event
     * @param handler - function that runs the process
     */
    static start (e, handler) {

		const button = e.target;
		const stopButton = document.querySelector('[data-action="stop"]');

        button.setAttribute('hidden', 'hidden');
		stopButton.removeAttribute('hidden');

        handler(e);

    }

    /**
     * Shows "Start" button and runs handler
     * @param {Event} e - "click" event
     * @param handler - function that runs the process
     */
    static stop (e, handler) {

        const button = e.target;
        const startButton = document.querySelector('[data-action="start"]');

        button.setAttribute('hidden', 'hidden');
        startButton.removeAttribute('hidden');

        handler(e);

    }

    /**
     * Sets listeners on button click
     * @param {string} event - event string (action data-attribute, method to call)
     * @param handler - function that runs the process
     */
	listen (event, handler) {

		const btn = document.querySelector(`[data-action="${event}"`);

		if (btn !== null) {

            const action = App[event];

		    btn.addEventListener('click', (e) => {

		        if (typeof action === 'function') {

                    action(e, handler);

                } else {

		            handler(e);

                }

            });

        }

		return this;
	}
}

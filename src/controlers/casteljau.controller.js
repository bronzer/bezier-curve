export class Casteljau {

	constructor () {

		/** coefficient of casteljau movement within 0 to 1 */
		this.t = 0;
		this.isOn = false;
		this._makeMove = () => {
		};

	}

	onCasteljauMove (fn) {

		if (typeof fn === 'function') {

			this._makeMove = fn;

		}

	}

	startCasteljauMovement () {

		this.isOn = true;

		this._startCasteljauMovement();

	}

	stopCasteljauMovement () {

		this.isOn = false;

	}

	resetCasteljauMovement () {

		this.t = 0;

		this._makeMove(0);

	}

	_startCasteljauMovement () {

		if (this.t > 100) {

			this.t = 0;

		}

		setTimeout(() => {

			const t = this.t / 100;

			this._makeMove(t);

			this.t += 0.1;

			if (this.isOn) {

				this._startCasteljauMovement();

			}

		});

	}

}

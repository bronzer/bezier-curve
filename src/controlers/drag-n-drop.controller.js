export class Dropper {

	static setListener (element, callback) {

		let binnedToPointFn;

		element.addEventListener('mousedown', () => {

			binnedToPointFn = (e) => callback(e, element);

			document.addEventListener('mousemove', binnedToPointFn);

		});

		document.addEventListener('mouseup', () => {

			document.removeEventListener('mousemove', binnedToPointFn);

		});

	}

}

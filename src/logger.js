import { CONSTANTS } from './common.js';


/**
 * Logger is component that shows current location of cursor
 */
export class Logger {

    constructor () {

        this.element = null;

    }

    /**
     * Initialize the process
     */
    init () {

        this.createLoggerBox();
        this.setMouseListener();

    }

    /**
     * Creates an element wrapper for logger
     */
    createLoggerBox () {

        this.element = document.createElement('div');

        Object.assign(this.element.style, CONSTANTS.LOGGER);

        document.body.appendChild(this.element);

    }

    /**
     * Set on mousemove listener
     */
    setMouseListener () {

        window.addEventListener('mousemove', (e) => this.onMouseMove(e));

    }

    /**
     * On mouse move handler
     * @param {MouseEvent} e - mouse move event
     */
    onMouseMove (e) {

        if (this.element !== null) {

            this.element.innerHTML = `x: ${e.clientX} <br> y: ${e.clientY}`;

        }

    }

}

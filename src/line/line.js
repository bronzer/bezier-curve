import { BezierElement } from '../bezier/bezier-element.js';
import { CONSTANTS } from '../common.js';


export class Line extends BezierElement {

	get length () {

		return this._element.getTotalLength();

	}

	get position () {

		const elementTotalLength = this._element.getTotalLength();

		const { x, y } = this._element.getPointAtLength(0);
		const { x: x2, y: y2 } = this._element.getPointAtLength(elementTotalLength);

		return { x, y, x2, y2 };

	}

	set position (coordinates) {

		const { x = this.position.x, y = this.position.y, x2 = this.position.x2, y2 = this.position.y2 } = coordinates;

		this._element.setAttributeNS(null, 'd', `M${x} ${y} L ${x2} ${y2}`);

	}

	constructor (point, nextPoint, props = {}) {

		super({ ...CONSTANTS.LINE, ...props });

		this.point = point;
		this.nextPoint = nextPoint;
		this._element = this._createElement();
		this._setLinePosition();

		this.point.onPositionChange((x, y) => {
			this.position = { x, y };
		});

		this.nextPoint.onPositionChange((x2, y2) => {
			this.position = { x2, y2 };
		});

	}

	getPointAtLength (length = 0) {

		return this._element.getPointAtLength(length);

	}

	_setLinePosition () {

		const { x, y } = this.point.position;
		const { x: x2, y: y2 } = this.nextPoint.position;

		this.position = { x, y, x2, y2 };

	}

	_createElement () {

		const line = document.createElementNS(CONSTANTS.SVG_NS, 'path');

		line.setAttribute('stroke', this._renderProps.strokeColor);
		line.setAttribute('stroke-width', `${this._renderProps.strokeWidth}`);

		return line;

	}

}

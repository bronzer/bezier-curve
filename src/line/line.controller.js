import { Line } from './line.js';
import { GuideLine } from './guide-line.js';


export class LineController {

	static removeLines (lines) {

		lines.forEach(line => line.destroy());

	}

	static createLine (point, nextPoint) {

		return new Line(point, nextPoint);

	}

	static createGuideLine (point, nextPoint) {

		return new GuideLine(point, nextPoint);

	}

}

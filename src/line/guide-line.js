import { Line } from './line.js';
import { CONSTANTS } from '../common.js';


export class GuideLine extends Line {

	constructor (point, nextPoint, props = {}) {

		super(point, nextPoint, {
			...CONSTANTS.GUIDE_LINE,
			...props,
		});

	}

}
